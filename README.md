# gitlab-mirror-to

Mirrors repositories from a local Gitlab instance to other git remotes. The local Gitlab is considered canonical.

To use:

* Ensure you have Perl 5.16 or later installed.
* Clone this repo somewhere on your Gitlab server.
* Create a config.txt in this directory (see below).
* Generate an id_rsa, put it in this directory.
  * Ensure that a user with that SSH key can push to all the remotes in your config.txt. You might have to disable protected branches, if the remotes go to a Gitlab server.
* Add a cron job to run mirror.pl on a regular basis.

## config.txt

This file tells gitlab-mirror-to what repos to mirror. It has the following format:

* A ```#``` character begins a comment
* Blank lines are ignored
* Lines like ```key = value``` are configuration settings. The following keys are supported:
  * repo_dir: Required, defines where on the filesystem Gitlab stores its repositories. If you installed Gitlab using [the default Omnibus installation method](https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/README.md), it should be set to ```/var/opt/gitlab/git-data/repositories```.
* Lines like ```org/project: remote``` define git repositories to backup. Before the colon is a path to a Gitlab repository, and after it is a git remote (eg: ```git@github.com:org/project.git```).
