#!/usr/bin/env ruby
require 'gitlab'
require 'tmpdir'

# Simple script to create all the destination repos for mirroring.
# Requires the gitlab gem
#
# Call as: ./create-repos.rb GITLAB_TOKEN MACHINE_USER

def parse_config(file)
  server = nil
  repos = []
  open(file) do |f|
    f.each_line do |line|
      md = /^([^:]*)\s*:\s*(\S+)$/.match(line) or next
      repo = md[2]
      mdr = %r{^(?:http(?:s)?://|\w+@)([^:/]+)[:/]([^/]+)/([^/]+?)(?:\.git)?$} \
        .match(repo) or next
      server ||= mdr[1]
      repos << [mdr[2], mdr[3]]
    end
  end

  return [server, repos]
end

def setup_gitlab(server, token)
  Gitlab.configure do |conf|
    conf.endpoint = "https://#{server}/api/v3"
    conf.private_token = token
  end
end

def find_user(name)
  Gitlab.users(:search => name).find { |u| u.username == name } \
    or raise "Can't find user #{name}"
end

def ensure_project(group_name, proj_name)
  # Check if it already exists
  group = Gitlab.groups.find { |g| g.path == group_name }
  group = Gitlab.group(group.id)
  proj = group.projects.find { |p| p['name'] == proj_name }

  # If it doesn't exist, create it
  if proj
    Gitlab.project(proj['id'])
  else
    Gitlab.create_project(proj_name, :namespace_id => group.id)
  end
end

def add_member(proj, user)
  access_level = 30 # Developer
  Gitlab.add_team_member(proj.id, user.id, access_level)
end

# Make sure the project has at least one branch
def ensure_branches(proj)
  return unless Gitlab.branches(proj.id).empty?

  # Create a dummy branch
  Dir.mktmpdir do |dir|
    Dir.chdir(dir) do
      system('git', 'init', '--quiet')
      IO.write('dummy', 'dummy')
      system('git', 'add', 'dummy')
      system('git', 'commit', '--quiet', '-m', 'dummy')
      system('git', 'push', '--quiet', proj.ssh_url_to_repo, 'master')
    end
  end
end

def unprotect_branches(proj)
  ensure_branches(proj)
  Gitlab.branches(proj.id).each do |branch|
    next unless branch.send(:protected)
    Gitlab.unprotect_branch(proj.id, branch.name)
  end
end

def setup_project(user, group_name, proj_name)
  puts proj_name
  proj = ensure_project(group_name, proj_name)
  # TODO: Should tag with "mirror", but no API support for project-tags
  add_member(proj, user)
  unprotect_branches(proj)
end

def create_repos(token, user)
  configfile = File.join(File.dirname(__FILE__), 'config.txt')
  server, repos = parse_config(configfile)
  setup_gitlab(server, token)
  user = find_user(user)
  repos.each { |g, p| setup_project(user, g, p) }
end

create_repos(*ARGV)
