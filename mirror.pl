#!/usr/bin/perl
use v5.16;
use warnings;
use autodie;

use File::Basename qw(dirname);
use File::Spec::Functions qw(catfile);
use Cwd qw(realpath getcwd);
use JSON::PP;
use Data::Dumper;

sub local_file {
  my $path = shift;
  return realpath(catfile(dirname(__FILE__), $path));
}

sub read_config {
  my %conf;
  my %repos;

  open my $config, local_file('config.txt');
  while (my $line = <$config>) {
    $line =~ s/#.*//;         # Remove comments
    next if $line =~ /^\s*$/; # Remove blank lines

    $line =~ /^(?<key>[^:\s]+) \s* (?<sym>[:=]) \s* (?<val>\S+) \s*$/x or
      die "Can't understand '$line'\n";
    # Equals means a conf var, colon means a repo
    my $hash = ($+{sym} eq ':' ? \%repos : \%conf);
    $hash->{$+{key}} = $+{val};
  }
  close $config;

  return (\%conf, \%repos);
}

sub git_push {
  my ($dir, $remote) = @_;
  my $cwd = getcwd;

  eval {
    local $ENV{GIT_SSH} = local_file('git-ssh.sh');
    chdir($dir);

    system('git', 'push', '--all', '--force', '--quiet', $remote);
  };
  print STDERR $@ if $@;

  chdir $cwd;
}

sub run {
  my ($conf, $repos) = read_config();
  while (my ($project, $remote) = each %$repos) {
    my $dir = catfile($conf->{repo_dir}, $project . '.git');
    next unless -d $dir; # Skip ones that don't exist

    git_push($dir, $remote);
  }
}

run();
